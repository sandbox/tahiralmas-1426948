// $Id: CheckPaymentOptions.js,v 1.0 2009/08/16 13:37:41 wla Exp jQuery
/**
 * Show only relevant selection and use
 * jQuery to display them.
 */
// Our namespace:
var CheckPaymentOptions = CheckPaymentOptions || {};

// In Drupal 7: ($ === jQuery)

CheckPaymentOptions.init = function() {
  setDefaultOptions();
  handleAddress("b");

  jQuery("#edit-panes-billing-billing-country").change(function() {
    handleAddress("b");
  });
  
  jQuery("#edit-panes-billing-billing-address-select").change(function() {
    setTimeout("handleAddress('b')", 100);
  });

  jQuery("#edit-panes-billing-copy-address").click(function() {
    var t = setTimeout("doIfChecked()", 100);
  }); 
};
  
  function handleAddress(paneFrom) {
    if (paneFrom == "b") {
      var country = parseInt(jQuery("#edit-panes-billing-billing-country").val());
    } else {
      var country = parseInt(jQuery("#edit-panes-delivery-delivery-country").val());
    }
    setDefaultOptions();
    showForCountry(country);
  };

  function doIfChecked() {
    var checked = jQuery("#edit-panes-billing-copy-address").attr("checked");
    if (checked) {
      handleAddress("d");
    } else {
      handleAddress("b");
    }
  };
  
  function setDefaultOptions() {
    jQuery('input[id*=moneybookers]').parent().hide();
    jQuery('input[id*=moneybookers-wrapper]').parent().show();
    jQuery('input[id*=moneybookers-cc]').parent().show();
  };
  
  function showForCountry(num) {
    switch(num) {
      case 276:
        jQuery('input[id*=moneybookers-sft]').parent().show();
        jQuery('input[id*=moneybookers-did]').parent().show();
        jQuery('input[id*=moneybookers-gir]').parent().show();
        break;
      case 56:
        jQuery('input[id*=moneybookers-sft]').parent().show();
        break;
      case 250:
        jQuery('input[id*=moneybookers-sft]').parent().show();
        jQuery('input[id*=moneybookers-gcb]').parent().show();
        break;
      case 528:
        jQuery('input[id*=moneybookers-sft]').parent().show();
        jQuery('input[id*=moneybookers-idl]').parent().show();
        break;
      case 756:
        jQuery('input[id*=moneybookers-sft]').parent().show();
        break;
      case 826:
        jQuery('input[id*=moneybookers-sft]').parent().show();
        jQuery('input[id*=moneybookers-vsd]').parent().show();
        jQuery('input[id*=moneybookers-mae]').parent().show();
        jQuery('input[id*=moneybookers-slo]').parent().show();
        jQuery('input[id*=moneybookers-vse]').parent().show();
       break;
      case 40:
        jQuery('input[id*=moneybookers-sft]').parent().show();
        jQuery('input[id*=moneybookers-eps]').parent().show();
        jQuery('input[id*=moneybookers-mae]').parent().show();
        break;
      case 208:
        jQuery('input[id*=moneybookers-dnk]').parent().show();
        break;
      case 702:
        jQuery('input[id*=moneybookers-ent]').parent().show();
        break;
      case 100:
        jQuery('input[id*=moneybookers-epy]').parent().show();
        break;
      case 246:
        jQuery('input[id*=moneybookers-so2]').parent().show();
        break;
      case 752:
        jQuery('input[id*=moneybookers-ebt]').parent().show();
        break;
      case 380:
        jQuery('input[id*=moneybookers-psp]').parent().show();
        jQuery('input[id*=moneybookers-csi]').parent().show();
        jQuery('input[id*=moneybookers-vse]').parent().show();
       break;
      case 616:
        jQuery('input[id*=moneybookers-pwy]').parent().show();
        break;
      case 372:
        jQuery('input[id*=moneybookers-lsr]').parent().show();
        break;
      case 616:
        jQuery('input[id*=moneybookers-pwy]').parent().show();
        break;
      case 36:
      case 554:
      case 710:
        jQuery('input[id*=moneybookers-pli]').parent().show();
        break;
      case 724:
        jQuery('input[id*=moneybookers-vse]').parent().show();
        break;
    }
  };

jQuery(document).ready(CheckPaymentOptions.init);
