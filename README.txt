// $ID: README.txt $

uc_moneybookers provides an payment interface for the Moneybookers payment service.
It is firstly written for an german shop solution and sets EUR as the default currency and DE or DEU as the default country code. The shop should install the country api and multicurrency in Europe in order to have a currency block in the order data structure.

The API of Moneybookers allows more details for amount and description to be transferred to their site to be shown to the customer during the payment process. These details are not available in the order data structure. If someone wants to make use of it, a small module could implement two blocks in the order structure
amount_block: 
an array of 6 values (amount_block[0] ... amount_block[5]).
These values correspond with the values amount2_description, amount2 to amount4_description .....

detail_block:
an array of 10 values (detail_block[0] ... detail_block[9]).
These values correspond with the values detail1_description, detail1_text to detail5_description .....
As detail1_description and detail1_text are mandatory to Moneybookers, $order->products[0]->title and $order->products[0]->model are used as default values.

For more details have a look into the moneybookers_gateway_manual and the code.

Finally, the standard form names "uc_cart_checkout_review_form" and "uc_cart_checkout_form" used in "uc_moneybookers_form_alter" and therefore must remain. 
Altering "uc_cart_checkout_form" just adds a jquery script for payment display related to billing country.
Altering "uc_cart_checkout_review_form" puts in the setup for sending to Moneybookers.